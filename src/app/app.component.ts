import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

import { Router } from '@angular/router';
import { ApiService } from './services/api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oneSignal: OneSignal,
    private alertCtrl: AlertController,
    private router : Router,
    private apiService : ApiService,
    private storage : Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      
      if (this.platform.is('cordova')){
        this.setupPush();
      }

      this.router.navigateByUrl('login');
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      
    });
  }

  async setupPush(){


    this.oneSignal.startInit('02ca3a81-d411-4cda-ba04-33e25e995a03', '999210961350');

    var status = await this.oneSignal.getPermissionSubscriptionState();

    this.storage.set('pushToken', status.subscriptionStatus.userId);

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);

    this.oneSignal.handleNotificationReceived().subscribe(data => {
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    this.oneSignal.handleNotificationOpened().subscribe(data => {
      let additionalData = data.notification.payload.additionalData;
      this.showAlert('Notificación abierta', 'Estás leyendo una notificación de Youneed.', additionalData.task);
    });



    this.oneSignal.endInit();
  }


  async showAlert(title, msg, task){
    const alert = await this.alertCtrl.create({
      header : title,
      subHeader : msg,
      buttons:[
        { 
          text:`Acción: ${task}`,
          handler: () => {
            //Ej navegar a un especifica pantalla
          }
        }
      ]
    })
  }
}
