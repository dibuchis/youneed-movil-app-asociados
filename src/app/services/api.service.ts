import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  proxyurl = 'http://localhost:8100/api/';
  url = 'https://app.youneed.com.ec/api/';
  ajaxurl = 'https://app.youneed.com.ec/ajax/';
  apiKey = ''; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }


  testApi() {
  
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  
  }

  /**
  * Login to Youneed Api 
  * map the result to return only registered user
  * 
  * @param {string} username 
  * @param {string} password 
  * @returns Observable with the search results
  */
 login(username: string, password: string): Observable<any> {
  
    let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
    let postData = "email=" + username + "&clave=" + password;
    return this.http.post(`${this.url}login`, postData, config).pipe(
      map(result => result)
    );
  }

  /**
  * Return pedidos 
  * map the result to return pedidos
  * 
  * @param {number} id 
  * @param {number} offset 
  * @returns Observable with the search results
  */
  getPedidos(id: number, offset: number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "uid=" + id + "&limit=10" + "&offset=" + offset;

  return this.http.post(`${this.url}getpedidos`, postData, config).pipe(
       map(result =>  result)
   );
  }

    /**
  * Return notificaciones 
  * map the result to return notifications
  * 
  * @param {number} id 
  * @param {number} page 
  * @returns Observable with the search results
  */
 getNotificaciones(id: number, page: number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "uid=" + id + "&page=" + page;

  return this.http.post(`${this.url}getnotificaciones`, postData, config).pipe(
       map(result =>  result)
   );
  }

  

  /**
  * Return Cliente Details from id Pedido
  * map the result to return multiple
  * 
  * @param {number} pedido_id 
  * @returns Observable with the search results
  */
 getCliente(pedido_id: number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "pedido_id=" + pedido_id;

  return this.http.post(`${this.url}getinfoclient`, postData, config).pipe(
       map(result =>  result)
   );
  }


  /**
  * Return Count of pedidos
  * map the result to count pedidos
  * 
  * @param {number} user_id 
  * @returns Observable with the search results
  */
 contarPedidos(user_id: number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + user_id;

  return this.http.post(`${this.url}contarpedidos`, postData, config).pipe(
       map(result =>  result)
   );
  }

  /**
  * Return User Details from email and token
  * map the result to return multiple
  * 
  * @param {string} email 
  * @param {string} token 
  * @returns Observable with the search results
  */
 getUserData(email: string, token:string): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "email=" + email + "&token=" + token;

  return this.http.post(`${this.url}getuserdata`, postData, config).pipe(
       map(result =>  result)
   );
  }


  /**
  * Return User Services
  * map the result to return multiple
  * 
  * @param {number} asociado_id
  * @returns Observable with the search results
  */
 getSetvicios(asociado_id: number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "asociado_id=" + asociado_id;

  return this.http.post(`${this.url}getservices`, postData, config).pipe(
       map(result =>  result)
   );
  }


  /**
  * Excecute aceptarPedido 
  * map the result to return aceptarPedido
  * 
  * @param {number} id 
  * @param {number} estado_previo 
  * @returns Observable with the search results
  */
 aceptarPedido(id: number, estado_previo : number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + id + "&estado_previo=" + estado_previo + "&estado_actual=" + 1 + "&action_executor=pedido";

  return this.http.post(`${this.url}aceptarpedido`, postData, config).pipe(
       map(result =>  result)
   );
  }

  
  /**
  * Excecute rechazarPedido 
  * map the result to rechazarPedido
  * 
  * @param {number} id 
  * @param {number} estado_previo 
  * @returns Observable with the search results
  */
 rechazarPedido(id: number, estado_previo : number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + id + "&estado_previo=" + estado_previo + "&estado_actual=" + 2 + "&action_executor=pedido";

  return this.http.post(`${this.url}rechazarpedido`, postData, config).pipe(
       map(result =>  result)
   );
  }

  /**
  * Excecute ejecutarPedido 
  * map the result to ejecutarPedido
  * 
  * @param {number} id 
  * @param {number} estado_previo 
  * @returns Observable with the search results
  */
 confirmarPedido(id: number, estado_previo : number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + id + "&estado_previo=" + estado_previo + "&estado_actual=" + 3 + "&action_executor=pedido";

  return this.http.post(`${this.url}confirmarpedido`, postData, config).pipe(
       map(result =>  result)
   );
  }

  /**
  * Excecute terminarPedido 
  * map the result to terminarPedido
  * 
  * @param {number} id 
  * @param {number} estado_previo 
  * @returns Observable with the search results
  */
 terminarPedido(id: number, estado_previo : number): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + id + "&estado_previo=" + estado_previo + "&estado_actual=" + 4 + "&action_executor=pedido";

  return this.http.post(`${this.url}terminarpedido`, postData, config).pipe(
       map(result =>  result)
   );
  }


  /**
  * Basic Update Profile 
  * map the result to actualizarPerfil
  * 
  * @param {string} email 
  * @param {string} nombres 
  * @param {string} apellidos 
  * @param {string} identificacion 
  * @param {string} numero_celular 
  * @param {string} telefono_domicilio 
  * @returns Observable with the search results
  */
 actualizarPerfil(email:string,nombres:string,apellidos:string,identificacion:string, numero_celular:string, telefono_domicilio:string): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "email=" + email + "&nombres=" + nombres + "&apellidos=" + apellidos + "&identificacion=" + identificacion + "&numero_celular=" + numero_celular + "&telefono_domicilio=" + telefono_domicilio;

  return this.http.post(`${this.url}appupdateprofile`, postData, config).pipe(
       map(result =>  result)
   );
  }

  /**
  * Basic Update Profile 
  * map the result to actualizarPerfil
  * 
  * @param {string} nombres 
  * @param {string} apellidos 
  * @param {string} identificacion 
  * @param {string} numero_celular 
  * @param {string} email 
  * @param {string} clave 
  * @returns Observable with the search results
  */
  registrar(nombres:string,apellidos:string,identificacion:string, numero_celular:string, telefono_domicilio:string, clave:string): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "tipo=Asociado&nombres=" + nombres + "&apellidos=" + apellidos + "&identificacion=" + identificacion + "&numero_celular=" + numero_celular + "&telefono_domicilio=" + telefono_domicilio + "&clave=" + clave;

  return this.http.post(`${this.url}register`, postData, config).pipe(
       map(result =>  result)
   );
  }

  /**
  * Register One Signal Push Token
  * map the result to regitrarPushToken
  * 
  * @param {number} id 
  * @param {string} token 
  * @returns Observable with the search results
  */
 registrarPushToken(id:number, token:string): Observable<any> {
  
  let config ={ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
  let postData = "id=" + id + "&token=" + token;

  return this.http.post(`${this.url}appregistrarpushtoken`, postData, config).pipe(
       map(result =>  result)
   );
  }
  
}