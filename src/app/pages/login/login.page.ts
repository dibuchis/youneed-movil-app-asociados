import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Spinner } from 'spin.js';
import Swal from 'sweetalert2';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  result: Observable<any>;
  login = {
    username:  "",
    password:  ""
  }
  errormsg: string = "";
  row_data: any = []; // Table rows
  spinner: Spinner;
  can_login: boolean;

  constructor(
    private apiService : ApiService, 
    private storage : Storage, 
    private router : Router
    ) { }

  async ngOnInit() {
    this.can_login = false;

    this.spinner = new Spinner({lines:9, scale : 0.45, length : 18, width:17,  radius: 44, color:"#ededed", fadeColor: '#126565', shadow : '0 0 3px #fff' });
    this.spinner.spin(document.querySelector('body'));
      var username = await this.storage.get('username');
      var password = await this.storage.get('password');
      
      await this.apiService.login(username, password).subscribe(
        result => {
            if(result['status']){
              this.storage.set('user', result['data']);
              this.spinner.stop();
              this.router.navigate(['/pedidos']);
            }else{
              this.spinner.stop();
              this.can_login = true;
            }
        }
      );
      
  }

  async submit() {
      
    if(this.login.username == '' || this.login.password == ''){
      Swal.fire({
        title:"Error",
        type:"info",
        text:"Por favor no deje campos vacios."
      });
      return null;
    }

    
    this.spinner.spin(document.querySelector('body'));
    this.errormsg = "";

    // Call our service function which returns an Observable
    this.apiService.login(this.login.username, this.login.password).subscribe(
      async result => {

          if(result['status']){
            this.storage.set('user', result['data']);
            
            this.storage.set('username', this.login.username);
            this.storage.set('password', this.login.password);
            this.spinner.stop();
            let pushtoken = await this.storage.get('pushToken');
            this.apiService.registrarPushToken(result['data'].usuario.id, pushtoken).subscribe((data) => {});
              
            this.router.navigate(['/pedidos']);

      }else{
        this.spinner.stop();
        Swal.fire({
          title:"Error",
          type:"warning",
          text:"Lo sentimos sus datos son incorrectos."
        });
      }
    }
    );
  }


  ngOnDestroy() { 
    if(this.spinner !== undefined){
      this.spinner.stop();
    }
  }


    

}
