import { ApiService } from './../../services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { IonInfiniteScroll } from '@ionic/angular';
import Swal from 'sweetalert2';
import { Spinner } from 'spin.js';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  pedidos = [];
  offset: number = 0;
  count: number = 0;
  scrollmsg;
  alert: any;
  color=['', '', 'danger', '', 'medium'];

  idmod = 0;
  toastMsg = "";
  numero_pedidos = 0;

  constructor(
    private storage : Storage, 
    private apiService : ApiService,
    private router : Router
    ) { }
  
  ngOnInit() {
  }

  ionViewWillEnter() {
    this.offset = 0;
    this.pedidos = [];
    this.loadData(event);
  }

  async loadData(event: any){
    await this.storage.get('user').then(obj => {
      this.apiService.contarPedidos(obj.usuario.id).subscribe(
        (data) => {
          this.numero_pedidos = data.data.numero_pedidos;  
        }
      );

      this.apiService.getPedidos(obj.usuario.id, this.offset).subscribe(
        (data) => {
          //console.log(data);
          //console.log(this.storage.get('user'));
          if(data.pedidos.length > 0){
            for (let f = 0; f < data.pedidos.length; f++) {
              this.pedidos.push(data.pedidos[f]);
              this.count++;
            }
            this.offset = this.offset + 10;
            try{
              event.target.complete();
            }catch(error){
              //todo put some error control
            }
          }else{
            event.target.disabled = true;
            this.scrollmsg = 'No tiene más pedidos por el momento.';
          }
        }
      );

      
    });
  }

  


  async aceptarPedido(id:number, previo:number){
    this.idmod = id;
    this.toastMsg = "El pedido #" + id + " ha sido aceptado.";
    this.alert = Swal.fire({
        title: "Aceptando Pedido #" + id,
        html:"Se está procesando su solicitud..",
        backdrop:false,
        onBeforeOpen: () => {
          Swal.showLoading();
        }
    });
    await setTimeout(function(){},3000);
    this.apiService.aceptarPedido(id, previo).subscribe(
      (data) => {
        this.alert.closeModal();
          if(data.status == 1){
              // this.router.navigate(['/pedidos']);
              //window.location.reload();
              this.reloadPage();
          }else{
              Swal.fire({
                type:"error",
                title:"Error",
                html:data.message
              })
          }
      }, 
      error => {
        this.alert.closeModal();
        Swal.fire({
          title:"Error",
          html:"Hubo un error, por favor vuelva a intentarlo."
        })
      }
    )
    

  }

  async rechazarPedido(id:number, previo:number){
    
    this.idmod = id;
    this.toastMsg = "El pedido #" + id + " ha sido rechazado.";
    this.alert = Swal.fire({
        title: "Rechazando Pedido #" + id,
        html:"Se está procesando su solicitud...",
        backdrop:false,
        onBeforeOpen: () => {
          Swal.showLoading();
        }
    });
    await setTimeout(function(){},3000);
    this.apiService.rechazarPedido(id, previo).subscribe(
      (data) => {
        this.alert.closeModal();
          if(data.status == 1){
              // this.router.navigate(['/pedidos']);
              //window.location.reload();
              this.reloadPage();
          }else{
              Swal.fire({
                type:"error",
                title:"Error",
                html:data.message
              })
          }
      }, 
      error => {
        this.alert.closeModal();
        Swal.fire({
          title:"Error",
          html:"Hubo un error, por favor vuelva a intentarlo."
        })
      }
    )

  }

  async confirmarPedido(id:number, previo:number){
    
    this.idmod = id;
    this.toastMsg = "El pedido #" + id + " está en ejecución.";
    this.alert = Swal.fire({
        title: "Ejecutando Pedido #" + id,
        html:"Se está procesando su solicitud...",
        backdrop:false,
        onBeforeOpen: () => {
          Swal.showLoading();
        }
    });
    await setTimeout(function(){},3000);
    this.apiService.confirmarPedido(id, previo).subscribe(
      (data) => {
        this.alert.closeModal();
          if(data.status == 1){
            // this.router.navigate(['/pedidos']);
            // window.location.reload();
            this.reloadPage();
          }else{
              Swal.fire({
                type:"error",
                title:"Error",
                html:data.message
              })
          }
      }, 
      error => {
        this.alert.closeModal();
        Swal.fire({
          title:"Error",
          html:"Hubo un error, por favor vuelva a intentarlo."
        })
      }
    )

  }

  async terminarPedido(id:number, previo:number){
    
    this.idmod = id;
    this.toastMsg = "El pedido #" + id + " está finalizado.";
    this.alert = Swal.fire({
        title: "Terminando Pedido #" + id,
        html:"Se está procesando su solicitud...",
        backdrop:false,
        onBeforeOpen: () => {
          Swal.showLoading();
        }
    });
    await setTimeout(function(){},3000);
    this.apiService.terminarPedido(id, previo).subscribe(
      (data) => {
        this.alert.closeModal();
          if(data.status == 1){
            // this.router.navigate(['/pedidos']);
            //window.location.reload();
            this.reloadPage();
          }else{
              Swal.fire({
                type:"error",
                title:"Error",
                html:data.message
              })
          }
      }, 
      error => {
        this.alert.closeModal();
        Swal.fire({
          title:"Error",
          html:"Hubo un error, por favor vuelva a intentarlo."
        })
      }
    )

  }

  reloadPage(){
    this.offset = 0;
    this.count = 0;
    this.pedidos = [];
    if(this.idmod > 0){
      this.alert = Swal.fire({
          type:"success",
          title: "Solicitud",
          html: this.toastMsg,
          backdrop:false
      })
      this.idmod = 0;
    }
    this.loadData(event);
  }

}
