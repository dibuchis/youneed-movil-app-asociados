import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Spinner } from 'spin.js';
import { Router } from '@angular/router';
import { ApiService } from './../../services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  user: any;
  spinner: Spinner;
  numero_pedidos = 0;

  constructor(private storage: Storage, private router: Router, private apiService: ApiService) { }

  async ngOnInit() {
    
  }

  async ionViewWillEnter() {
    await this.storage.get('user')
    .then( (data) => {

      this.apiService.contarPedidos(data.usuario.id).subscribe(
        (data) => {
          this.numero_pedidos = data.data.numero_pedidos;  
        }
      );

      this.apiService.getUserData(data.usuario.email, data.usuario.token).subscribe((result)=>{
          //console.log(result);
          this.user = result.data.usuario;
      });
      //console.log(this.user);
    });
  }

  async logout(){
    this.spinner = new Spinner({lines:9, scale : 0.45, length : 18, width:17,  radius: 44, color:"#ededed", fadeColor: '#126565', shadow : '0 0 3px #fff' });
    await this.storage.remove('user');
    await this.storage.remove('username');
    await this.storage.remove('password');
    //this.router.navigate(['/login']);
    window.location.replace("/");
  }

  async actualizarPerfil(){
    this.spinner = new Spinner({lines:9, scale : 0.45, length : 18, width:17,  radius: 44, color:"#ededed", fadeColor: '#126565', shadow : '0 0 3px #fff' });
    this.spinner.spin(document.querySelector('body'));

    await this.storage.get('user').then(obj => {
     this.apiService.actualizarPerfil(
          this.user.email, this.user.nombres, this.user.apellidos, this.user.identificacion, this.user.numero_celular, this.user.telefono_domicilio).subscribe(
        async (response) => {
          //console.log(data);

          if(response.status == 1){

            this.apiService.getUserData(obj.usuario.email, obj.usuario.token).subscribe((result)=>{
                this.user = result.data.usuario;
                this.spinner.stop();
            });
            
            Swal.fire({
              type:"success",
              html:response.message
            });
          }else{
            Swal.fire({
              type:"error",
              html:response.message
            });
          }
              
          this.spinner.stop();
        }
      );

      
    });
  }

}
