import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Spinner } from 'spin.js';
import { Router } from '@angular/router';
import { ApiService } from './../../services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage implements OnInit {

  servicios: any;
  numero_pedidos = 0;
  
  constructor(private storage: Storage, private router: Router, private apiService: ApiService) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    await this.storage.get('user')
    .then( (data) => {
      
      this.apiService.contarPedidos(data.usuario.id).subscribe(
        (data) => {
          this.numero_pedidos = data.data.numero_pedidos;  
        }
      );

      //console.log(data);
        this.apiService.getUserData(data.usuario.email, data.usuario.token).subscribe(
          (result) => {
            var servs = result.data.usuario.servicios;
            servs.forEach(function(serv, index, arr){

              var srvs = serv.incluye;
              srvs = srvs.replace("<p>", "");
              srvs = srvs.replace("</p>", "");
              var servicio_incluye = srvs.split(",");
              var no_srvs = serv.no_incluye;
              no_srvs = no_srvs.replace("<p>", "");
              no_srvs = no_srvs.replace("</p>", "");
              var servicio_no_incluye = no_srvs.split(",");
              serv.incluye = servicio_incluye;
              serv.no_incluye = servicio_no_incluye;
              
            })
            //console.log(result.data.usuario.servicios);
            this.servicios = servs;
        })


    })
  }

}
