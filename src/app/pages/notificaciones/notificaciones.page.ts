import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {

  nots = [];
  page: number = 1;
  count: number = 0;
  scrollmsg;
  numero_pedidos = 0;

  constructor(private storage : Storage, private apiService : ApiService) { }

  loadData(event){
    this.storage.get('user').then(obj => {

      this.apiService.contarPedidos(obj.usuario.id).subscribe(
        (data) => {
          this.numero_pedidos = data.data.numero_pedidos;  
        }
      );

      this.apiService.getNotificaciones(obj.usuario.id, this.page).subscribe(
        (data) => { 
          if(data.data.notificaciones.length > 0){
            for (let f = 0; f < data.data.notificaciones.length; f++) {
              this.nots.push(data.data.notificaciones[f]);
              this.count++;
            }
            this.page ++;
            try{
            event.target.complete();
            }catch(error){

            }
          }else{
            event.target.disabled = true;
            this.scrollmsg = 'No tiene más notificaciones';
          }
        }
      );

      
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.page = 0;

    this.loadData(event);
  }

}
