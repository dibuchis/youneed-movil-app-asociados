import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation
} from "@ionic-native/google-maps";
import { Platform, LoadingController, ToastController } from "@ionic/angular";

@Component({
  selector: 'app-cliente-detail',
  templateUrl: './cliente-detail.page.html',
  styleUrls: ['./cliente-detail.page.scss'],
})
export class ClienteDetailPage implements OnInit {
  
  id: any;
  marker: any;
  pedido: any;
  map: GoogleMap;
  loading: any;
  servicio_incluye: any;
  servicio_no_incluye: any;
  numero_pedidos = 0;

  constructor(
    public loadingCtrl: LoadingController,
    private apiService : ApiService, 
    private storage : Storage, 
    private route: ActivatedRoute,
    private platform: Platform
    ) { }

    ngOnInit() {
    }
  
    async ionViewWillEnter() {
    
    //await this.platform.ready();

    await this.storage.get('user').then(obj => {

      this.apiService.contarPedidos(obj.usuario.id).subscribe(
        (data) => {
          this.numero_pedidos = data.data.numero_pedidos;  
        }
      );

      this.route.params.subscribe(params => {
            this.id = params['id']; 

            this.apiService.getCliente(this.id).subscribe(
              (data) => {
                  this.pedido = data;
                  var srvs = data.servicio.incluye;
                  srvs = srvs.replace("<p>", "");
                  srvs = srvs.replace("</p>", "");
                  this.servicio_incluye = srvs.split(",");
                  var no_srvs = data.servicio.no_incluye;
                  no_srvs = no_srvs.replace("<p>", "");
                  no_srvs = no_srvs.replace("</p>", "");
                  this.servicio_no_incluye = no_srvs.split(",");
                  //console.log(data);
                  //console.log(data.pedido.latitud);
                  //console.log(data.pedido.longitud);
                  
                  this.loadMap(data.pedido.latitud, data.pedido.logitud);
              }
            );

      });
    });

    
  }

  loadMap(latitud: any, longitud: any) {
    // Esta función inicializa la propiedad de clase
    // map
    // que va a contener el control de nuestro mapa de google
    var centerLat = parseFloat(latitud).toFixed(0);
    var centerLng = parseFloat(longitud).toFixed(0);
    // Para crear nuestro mapa debemos enviar como parametros
    // el id del div en donde se va a renderizar el mapa (paso anterior)
    // y las opciones que configuran nuestro mapa
    this.map = GoogleMaps.create("map_canvas", {
        center: {
          lat: centerLat,
          lng: centerLng
        },
        zoom: 18,
        tilt: 30,
        gestureHandling: 'none',
        zoomControl: false
    });
  }

}
